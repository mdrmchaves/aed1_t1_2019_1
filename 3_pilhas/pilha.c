#include "pilha.h"
#include <stdlib.h>
#include "ls.h"
#define Mx 10

struct pilha * create_p(){
	struct list *ldesc = create(Mx);
	struct pilha *desc  = malloc(sizeof(struct pilha));
	desc->pilhas = ldesc;
	return desc;
}

int makenull(struct pilha * p){
	if(p!=NULL){	
	free(p->pilhas->arm);
	return 1;}
	else return 0;
}

int top(struct pilha * p){	
	if(p!=NULL)	
	return get(p->pilhas,p->pilhas->ultimo);
	else return 0;
}

int pop(struct pilha * p){
	if(p!=NULL)	
	return removel(p->pilhas,p->pilhas->ultimo);
	else return 0;
}

int push(struct pilha * p, int val){
	if(p!=NULL)	
	return insert(p->pilhas, val, length(p->pilhas)+1);
	else return 0;
}

int vazia(struct pilha *p){
	if(length(p->pilhas)==0) return 1;
	else return 0;
}	

void destroy_p(struct pilha *p){
	destroy(p->pilhas);
}
