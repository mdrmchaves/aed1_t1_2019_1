#include "le.h"
#include <stdlib.h>

struct llist * create_l(){
	struct llist * desc;
	desc = malloc((sizeof(struct llist))*1);
	desc->tam=0;
	desc->cabeca=NULL;
	return desc;
}

elem * create_node(int val){
	elem *novo;
	novo = malloc(sizeof(elem)*1);
	novo->val=val;
//	novo->next=NULL;
	return novo;
}

int insert_l(struct llist *desc, elem * prev, elem * item){
		if(prev == NULL){
			item->next = desc->cabeca;
			desc->cabeca = item;
						
			desc->tam++;	
			return 1;
		}
		else{
			elem *aux;
			aux = prev->next;
			prev->next = item;
			item->next = aux;
			desc->tam++;
			return 1;
		}
		return 0;
}

int delete_l(struct llist *desc, elem * prev){
	elem *aux;
		if(prev == NULL){
			aux = desc->cabeca->next;
			free(desc->cabeca);
			desc->cabeca = aux;
			return 0;
		}else{
			for(aux=desc->cabeca;aux!=prev && aux->next != NULL;){
				aux=aux->next;			
			}
			if(aux!=prev){
				aux->next = prev->next->next;
				free(prev->next);
				return 1;
			}else return 0;
		}	
}

elem *get_l(struct llist *desc, int pos){
	int i;	
	struct llist *aux = desc;
	if(pos>=length_l(desc)|| pos< 1) return NULL;
	else{
		for(i=0; i<length_l(desc);++i){
			if(i==pos-1) return aux->cabeca;
			
			aux->cabeca= aux->cabeca->next; 
		}
	}
	return NULL;
}

int set_l(struct llist *desc, int pos, int item){
	int i;	
	if(pos>0 && pos<length_l(desc)){
		for(i=0;i<pos;++i){
			if(i==pos-1){
				desc->cabeca->val = item;
				return 1;			
			desc->cabeca = desc->cabeca->next; 
			
			}
		}
		return 0;
	}
	return 0;
}

elem * locate_l(struct llist *desc, elem * prev, int val){
	/*if(prev == NULL) return locate_l(desc,desc->cabeca, val);	
	if(prev->val== val) return prev;
	if(prev->next == NULL) return NULL;
	return locate_l(desc,prev->next, val);*/
	struct elem * l;
	l=prev;
	if(prev == NULL){
		l = desc->cabeca;
	}
	while(l != NULL && val != l->val){
			l = l->next;	
		}
	
	
	
	return l;
}

int length_l(struct llist *desc){
	return desc->tam;
}

void destroy_l(struct llist *desc){
		free(desc);
}
