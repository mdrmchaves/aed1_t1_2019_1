#include "ls.h"
#include "stdlib.h"

struct list * create(int max){
	if(max>0){
	struct list *desc;
	desc = malloc((sizeof(struct list)) * 1);
	desc->arm = malloc(sizeof(int) * max);
	desc->ultimo = 0;
	desc->capacidade = max;	
	
	return desc;
	}
	return NULL;
}

int insert(struct list *desc, int pos, elem item){
     if(desc->ultimo+1 <= max(desc)){
	int i=0;
	desc->ultimo++;
	for(i =pos-1 ;i<=max(desc);i++){
			desc->arm[i] = desc->arm[i+1];
		}
		desc->arm[pos-1] = item;	
		return 1;
		}else {
		 return 0;
	 }
}

int removel(struct list *desc, int pos){
	if(pos-1 <= max(desc)){		
		int i;
		desc->ultimo--;		
			for(i=pos-1;i<=max(desc);i++){
				desc->arm[i] = desc->arm[i+1];		
		}
		return 1;	
	}else return 0;

}

elem get(struct list *desc, int pos){
	if((pos-1) <= length(desc) && (pos-1) >=0){
		return desc->arm[pos-1];
	}
	return 0;
}

int set(struct list *desc, int pos, elem item){
	if(pos <= length(desc) && pos >=0){
		desc->arm[pos-1] = item;
		return 1;	
	}
	else return 0;
}

int locate(struct list *desc, int pos, elem item){
	int i;
	if(pos-1 <= length(desc)){
		for(i=pos-1; i<=length(desc);i++){
			if(desc->arm[i] == item) return i+1;	
		}
	} 
	return 0;
}

int length(struct list *desc){
	return desc->ultimo;
}

int max(struct list *desc){
	return desc->capacidade;
}

int full(struct list *desc){
	if(length(desc) == max(desc)) return 1;
	else return 0; 
}

void destroy(struct list *desc){
	free(desc);
}
