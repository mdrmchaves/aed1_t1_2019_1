#include <stdlib.h>
#include <stdio.h>
#include "fila.h"
#define TFILA 10

struct fila * create_f(){
	int novo[TFILA], i;
	for(i=0;i<TFILA;i++) novo[i] = 0;
	struct fila * novaFila = malloc (sizeof(struct fila));
	novaFila->filas = novo;
	novaFila->primeiro = 0;
	novaFila->ultimo = 0;
	if(novaFila != NULL)
		return novaFila;
	else return NULL;
}

int makenull(struct fila * f){
	int i;	
	for(i=0;i<TFILA;i++) f->filas = 0;
	f->primeiro =0;
	f->ultimo=0;
	if(f->filas[0]==0) return 1;
	else return 0;
}

int dequeue(struct fila * f){
	int aux;	
	if(f->filas[f->ultimo]==0) return 0;
	else{
		aux = f->filas[f->ultimo];		
		f->filas[f->ultimo] = 0;
		f->ultimo=(f->ultimo+1)%TFILA;
		/*if((f->ultimo+1) == TFILA){
			f->ultimo = 0;
		}else f->ultimo++;*/
		
		return aux;
	}
}

int enqueue(struct fila * f, int val){
	if((f->primeiro+1) == f->ultimo || (f->primeiro == TFILA && f->ultimo == 0 )){
		return 0;		
	}else {
		if(f->primeiro == TFILA){
			if(f->ultimo == 0) return 0;

			else f->primeiro = 0;			

		}

		f->filas[f->primeiro] = val;		
		f->primeiro++;		
		return 1;
			
	}
	
}

int vazia(struct fila * f){
	/*if(f->primeiro == f->ultimo && f->ultimo == 0) return 1;
	else return 0;*/
	int i,aux;	
		for(i=0; i<TFILA;i++){
			if(f->filas[i] == 0) aux++;
		}
	if(aux == TFILA) return 1;
	else return 0;	
}

void destroy_f(struct fila * f){
	free(f);
}

/*
struct fila * create_f(){
	struct list * novo = create(10);
	struct fila * novaFila = malloc (sizeof(struct fila));
	novaFila->filas = novo;
	novaFila->primeiro = 0;
	novaFila->ultimo = 0;
	if(novaFila != NULL)
		return novaFila;
	else return NULL;
}

int makenull(struct fila * f){
	free(f->filas);
	struct list * novo = create(10);
	f->filas = novo;
	if(f->filas[0].arm ==NULL) return 1;
	else return 0;
}

int dequeue(struct fila * f){
	if(f->filas[f->ultimo].arm==NULL) return 0;
	else{
		if(f->ultimo == length(f->filas)){ 
			if(f->filas[0].arm !=NULL) 
				f->ultimo = 0; 
			return 0;
		}else f->ultimo++;
		f->filas[f->ultimo].arm = NULL;
		return f->filas[f->ultimo].arm;	
	}
}

int enqueue(struct fila * f, int val){
	if(full(f->filas) == 0){
		f->filas[f->primeiro].arm = val;
		return 1;
	}else {
		return 0;		
	}
	if(f->primeiro == length(f->filas)){
		if(f->ultimo == 0) return 0;
		else f->primeiro = 0;			
	}else f->primeiro++;
}

int vazia(struct fila * f){
	int i,aux;	
	for(i=0; i<length(f->filas);i++){
		if(f->filas[i].arm == 0) aux++;
	}
	if(aux == (length(f->filas)-1)) return 1;
	else return 0;

void destroy_f(struct fila * f){
	free(f);
}
}
*/
